#include <iostream>
#include "Pila.h"
using namespace std;

// menu
void menu(int pila[], int &tope, int cantidad, bool &band){
  // se inicia la clase
  Pila pi = Pila();
  int opcion;
  do {

    cout << "----------MENU----------" << endl;
    cout << "1.- Agregar" << endl;
    cout << "2.- Eleminar" << endl;
    cout << "3.- Ver pila" << endl;
    cout << "4.- Salir" << endl;
    cout << "------------------------" << endl;

    cout << "Ingrese una opcion: ";
    cin >> opcion;

    switch(opcion){
      case 1:
      pi.push(pila, tope, cantidad, band);
          break;
      case 2:
      pi.pop(pila, tope, cantidad, band);
          break;
      case 3:
      pi.imprimir_pila(pila, tope, band);
          break;
      }
   }while (opcion != 4);
}

int main() {
  int tope = 0;
  int cantidad;
  bool band;
  // se pide la cantidad de numeros que va a contener la pila
  cout << "Ingrese la cantidad de numeros ";
  cin >> cantidad;
  int pila[cantidad];
  menu(pila, tope, cantidad, band);

   return 0;
}
