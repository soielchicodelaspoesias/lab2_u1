#include <iostream>
#include "Pila.h"
using namespace std;

Pila::Pila() {
    int numero = 0;
    bool band;
}

Pila::Pila (int numero, bool band) {
    this->numero = numero;
    this->band = band;
}

void Pila::pila_llena(int tope, int cantidad, bool &band){
  if (tope == cantidad){
    band = true;
  }else{
    band = false;
  }
}

void Pila::push(int pila[], int &tope, int cantidad, bool band){
  pila_llena(tope, cantidad, band);
  // si band es true la pila esta llena
  if (band == true){
    cout << "Pila llena" << endl;
  }
  else{
    // ssino se pide en numero a agregar
    int numero;
    cout << "Ingrese un numero a agregar: ";
    cin >> numero;
    tope = tope + 1;
    pila[tope] = numero;
  }
}

void Pila::pila_vacia(int tope, bool &band){
  // si el tope es igual a uno quiere decir que la pila esta vacia
  if(tope==0){
    band = true;
  }else{
    band = false;
  }
}

void Pila::pop(int pila[], int &tope, int cantidad, bool band){
  // se elimina un elemnto si la pila no esta vacia
  pila_vacia(tope, band);
  if(band == true){
    cout << "Pila vacia" << endl;
  }else{
    cout << "Elemento Eliminado" << endl;
    tope = tope - 1;
  }
}

void Pila::imprimir_pila(int pila[], int tope, bool &band) {
  // si la pila esta vacia no imprime nada
  pila_vacia(tope, band);
  if(band==true){
    cout << endl;
    cout << "Pila vacia" << endl;
  }else{
    // sino se imprime cada elemto de la pila
    for(int i=tope;i>0;i--){
      cout << "| " << pila[i] << " |" << endl;
    }
  }
}
