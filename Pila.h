#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila {
    private:
      // metodos privados
        int numero = 0;
        bool band;

    public:
      // metodos publicos
        Pila ();
        Pila (int numero, bool band);
        void pila_llena(int tope, int cantidad, bool &band);
        void push(int pila[], int &tope, int cantidad, bool band);
        void pila_vacia(int tope, bool &band);
        void pop(int pila[], int &tope, int cantidad, bool band);
        void imprimir_pila(int pila[], int tope, bool &band);
};
#endif
